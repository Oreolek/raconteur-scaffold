situation = require('raconteur')
undum = require('undum-commonjs')
oneOf = require('raconteur/lib/oneOf.js')
qualities = require('raconteur/lib/qualities.js')
$ = require("jquery")

Array.prototype.oneOf = () ->
  oneOf.apply(null, this)

md = require('markdown-it')
markdown = new md({
  typographer: true,
  html: true
})

undum.game.id = "your-id-here"
undum.game.version = "1.0"

a = require('raconteur/lib/elements.js').a
way_to = (content, ref) -> a(content).class('way').ref(ref)
textlink = (content, ref) -> a(content).once().writer(ref)
actlink = (content, ref) -> a(content).once().action(ref)
textcycle = (content, ref) -> a(content).replacer(ref).class("cycle").id(ref).toString()
# usage: writemd( system, "Text to write")
writemd = (system, text) ->
  if typeof text is Function
    text = text()
  text = markdown.render(text)
  system.write(text)

preparemd = (text, mark) ->
  if typeof text is Function
    text = text()
  text = markdown.render(text)
  if mark?
    text = """
      <div class="#{mark}">
        #{text}
      </div>
    """
  return text

update_ways = (ways) ->
  content = ""
  for way in ways
    if undum.game.situations[way]?
      content += way_to(undum.game.situations[way].title, way)
  $("#ways").html(content)

situation "start",
  content: """
    The game starts here.
  """

is_visited = (situation) ->
  situations = undum.game.situations[situation]
  if situations
    return Boolean situations.visited
  return 0

# N-th level examine function
level = (text, mark) ->
  $("#content .#{mark}").fadeOut()
  return preparemd(text, mark)

lvl1 = (text) ->
  $("#content .lvl2").fadeOut()
  $("#content .lvl3").fadeOut()
  $("#content .lvl4").fadeOut()
  level(text, "lvl1")

lvl2 = (text) ->
  $("#content .lvl3").fadeOut()
  $("#content .lvl4").fadeOut()
  level(text, "lvl2")

lvl3 = (text) ->
  $("#content .lvl4").fadeOut()
  level(text, "lvl3")

lvl4 = (text) ->
  level(text, "lvl4")
