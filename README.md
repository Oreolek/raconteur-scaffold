# Raconteur scaffold

Oreolek's version.

### What's different
* SASS with v4-Bootstrap instead of LESS
* The character qualities are removed from the interface.
The interface is much cleaner this way.
* Lots of utility functions
* Gulpfile is written in CoffeeScript too
* Your game is split between several CoffeeScript files
* uses Oreolek's `undum-commonjs` build
* Custom "exits" interface block is shown
